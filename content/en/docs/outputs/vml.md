---
title: VML
description: Vector Markup Language.
params:
- vml
- vmlz
---
**Deprecated**: Removed from Graphviz on 2023-01-26.

Produces [VML](http://www.w3.org/TR/NOTE-VML) output,
the latter in compressed format.

VML is obsolete, superseded by [SVG](/docs/outputs/svg/).

See [ID Output Note](/docs/outputs/#ID).
